/*
This package provides a high-level Go API for TensorFlow, providing the
necessary tools to create and manipulate Tensors, Variables, Constants and
also to build, load and run Graphs.
*/
package tensorflow
